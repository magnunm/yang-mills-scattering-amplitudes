* Compute the squared Feynman amplitude for quark anti-quark
* anhilation into two gluons. Here replacing the sum over polarizations
* by the expression in the light cone gauge.
* For simplicity take here the quark masses to be zero from the start.
Off statistics;
Indices Lmu1, Lnu1, Lrho1, Lsig1, Lal1, Lbe1,
        Umu1, Unu1, Urho1, Usig1, Ual1, Ube1,
        Lmu2, Lnu2, Lrho2, Lsig2, Lal2, Lbe2,
        Umu2, Unu2, Urho2, Usig2, Ual2, Ube2,
	i1, ..., i5, j,
	a, b, c, d, e;
Set L: Lmu1, Lnu1, Lrho1, Lsig1, Lal1, Lbe1, Lmu2, Lnu2, Lrho2, Lsig2, Lal2, Lbe2;
Set U: Umu1, Unu1, Urho1, Usig1, Ual1, Ube1, Umu2, Unu2, Urho2, Usig2, Ual2, Ube2;
Vectors p1, p2, p3, p4, N, N3, N4, v, w, Sv, Tv, Uv;
Tensors g(symmetric), E, Ec, f(antisymmetric),
        Ns(symmetric), TR(cyclic);
* Needs to be non-commuting here because we supress fundamental rep indices.
NTensors T;
* sU, sV stands for the spinors u and v.
Functions sU, sUbar, sV, sVbar;
Symbols s, t, u, n, m, i;

* The amplitude divided by g^2:

Local Ms = -i * (T(c) * T(d) - T(d) * T(c))
      	   * Ns(Lal1, Lbe1) *
      	   (g(Urho1, Usig1) * (p3(Ube1) - p4(Ube1))
	   + g(Usig1, Ube1) * (p4(Urho1) - Sv(Urho1))
	   + g(Ube1, Urho1) * (Sv(Usig1) - p3(Usig1))) *
	   E(Lrho1, Lsig1) *
           sVbar(p2) * g_(1, Ual1) * sU(p1);

Local Mu = -i * T(c) * T(d) * E(Lrho1, Lsig1) *
           sVbar(p2) *
           g_(1, Urho1) * (g_(1, Uv)) * g_(1, Usig1) *
           sU(p1);

Local Mt = -i * T(d) * T(c) * E(Lrho1, Lsig1) *
           sVbar(p2) *
           g_(1, Usig1) * (g_(1, Tv)) * g_(1, Urho1) *
           sU(p1);

* In the complex conjugate amplitude the order of the color factors
* are reversed.  Also choose the contraction indices such that we get
* one upper and one lower index in the trace.

Local MsCC = i * (T(d) * T(c) - T(c) * T(d))
      	   * Ns(Ual2, Ube2) *
      	   (g(Lrho2, Lsig2) * (p3(Lbe2) - p4(Lbe2))
	   + g(Lsig2, Lbe2) * (p4(Lrho2) - Sv(Lrho2))
	   + g(Lbe2, Lrho2) * (Sv(Lsig2) - p3(Lsig2))) *
	   Ec(Urho2, Usig2) *
           sUbar(p1) * g_(1, Lal2) * sV(p2);

Local MuCC = i * T(d) * T(c) * Ec(Urho2, Usig2) *
             sUbar(p1) *
             g_(1, Lsig2) * (g_(1, Uv)) * g_(1, Lrho2) *
             sV(p2);

Local MtCC = i * T(c) * T(d) * Ec(Urho2, Usig2) *
             sUbar(p1) *
             g_(1, Lrho2) * (g_(1, Tv)) * g_(1, Lsig2) *
             sV(p2);

* Choose some gauge for the propagators:
*** Feynman gauge
id Ns(i1?, i2?) = g(i1, i2);

* Replace placeholder 4-vectors
id Sv = p1 + p2;
id Tv = p1 + p3;
id Uv = p1 + p4;

.sort

* The square:
Local square = (Ms/s + Mu/u + Mt/t) *
               (MsCC/s + MuCC/u + MtCC/t);

id i^2 = -1;

* Sum over spins
id sU(w?) * sUbar(w?) = g_(1, w);
id sV(w?) * sVbar(w?) = g_(1, w);

* Since we are sure that the expressions are now surrounded by
* spinors like uBar(?) * (...) * u(?)  with no internal spinors in
* (...) we can be sure the following works correctly:
id sUbar(w?) = g_(1, w);
id sU(w?) = 1;
id sVbar(w?) = g_(1, w);
id sV(w?) = 1;

* Take the trace:
Trace4 1;

* We chose the indices so that this is correct:
id d_(i1?, i2?) = g(i1, i2);

* And insert for the polarisation sum the expression in the
* light cone gauge.
id E(i1?, i2?) * Ec(i3?, i4?) =
   (g(i1, i3) - (p3(i1)*N3(i3) + p3(i3)*N3(i1)) / (p3.N3))
   *
   (g(i2, i4) - (p4(i2)*N4(i4) + p4(i4)*N4(i2)) / (p4.N4));

* Contract upper and lower indices:
repeat;
    id g(i1?, i2?L[n]) * v?(i3?U[n]) = v(i1);
    id g(i1?, i2?U[n]) * v?(i3?L[n]) = v(i1);
    id g(i1?, i2?L[n]) * g(i3?, i4?U[n]) = g(i1, i3);
    id g(i1?L[n], i2?U[n]) = 4;
    id v?(i1?L[n]) * w?(i2?U[n]) = v.w;
endrepeat;

* Now take the color trace (sum over final state color)
id T(i1?) * T(i2?) * T(i3?) * T(i4?) = TR(i1, i2, i3, i4);
id TR(i1?, i1?, i2?, i2?) = 16/3;
id TR(i1?, i2?, i1?, i2?) = - 2/3;

* In the light cone gauge the fixed vector is lightlike
id N3.N3 = 0;
id N4.N4 = 0;

* Simplify
* Since we have p3.N3 and p4.N4 in the denominators keep
* those vectors in the numerators also.
id p2 = - p1 - p3 - p4;
id v?{p4, p1, p3}.v?{p4, p1, p3} = 0;

id p1.p4^n? = (u/2)^n;
id p1.p3^n? = (t/2)^n;
id p3.p4^n? = (s/2)^n;

* These relations between N3, N4 and the 4-momenta p3, p4 is
* dervied by a explicit calculation in the CoM frame. There
* we see N3 must be paralel to p4 and vise versa.
id N3.p4 = 0;
id N4.p3 = 0;

* Give a shorter expression in our case:
id u^2 = t^2 + s^2 + 2*t*s;
id t^2 = s^2 + u^2 + 2*u*s;

bracket N3, N4;
Print square;
.end
