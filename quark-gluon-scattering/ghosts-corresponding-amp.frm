Indices Lmu1, Lnu1, Lrho1, Lsig1, Lal1, Lbe1,
        Umu1, Unu1, Urho1, Usig1, Ual1, Ube1,
        Lmu2, Lnu2, Lrho2, Lsig2, Lal2, Lbe2,
        Umu2, Unu2, Urho2, Usig2, Ual2, Ube2,
	c, d, e,
        i1, ..., i5;
Set L: Lmu1, Lnu1, Lrho1, Lsig1, Lal1, Lbe1, Lmu2, Lnu2, Lrho2, Lsig2, Lal2, Lbe2;
Set U: Umu1, Unu1, Urho1, Usig1, Ual1, Ube1, Umu2, Unu2, Urho2, Usig2, Ual2, Ube2;
Vectors p1, p2, p3, p4, N, v, w;
Tensors g(symmetric), E, Ec, f(antisymmetric),
        Ns(symmetric), TR(cyclic);
* Needs to be non-commuting here because we supress fundamental rep indices.
NTensors T;
* sU, sV stands for the spinors u and v.
Functions sU, sUbar, sV, sVbar;
Symbols s, t, u, xi, n, m, [u - m^2], i;

* The ghost amplitude divided by g^2. Here with the spin line incoming
* at p4.
Local ghostAmp = (T(c) * T(d) - T(d) * T(c)) * Ns(Lal1, Lbe1) * (p3(Ube1))*
                 sVbar(p2) * g_(1, Ual1) * sU(p1);

* The complex conjugated amplitude has the color factors reversed. A special
* thing about the ghosts is that it is the cross -term between the two spin
* line directions that cancels the contribution from longitudinal polarizations
* in the gluon amp. Therefore here we have the spin line incoming at p3.
Local ghostAmpCC = (T(d) * T(c) - T(c) * T(d)) * Ns(Ual2, Lbe2) *
                   (p4(Ube2)) * sUbar(p1) * g_(1, Lal2) * sV(p2);

bracket T;
Print ghostAmp;
.sort

* Multiply by 2 to add the squared amplitude with p4 in place of p3
* (the other orientation of the "spin line") since this second
* amplitude has the same square:
Local square = 2 * ghostAmp * ghostAmpCC / (s^2);

* Sum over spins
id sU(w?) * sUbar(w?) = g_(1, w) + m;
id sV(w?) * sVbar(w?) = g_(1, w) - m;

* Since we are sure that the expressions are now surrounded by spinors
* like uBar(?) * (...) * u(?)  with no internal spinors in (...) we
* can be sure the following works correctly:
id sUbar(w?) = g_(1, w) + m;
id sU(w?) = 1;
id sVbar(w?) = g_(1, w) - m;
id sV(w?) = 1;

* Take the trace:
Trace4 1;

* We chose the indices so that this is correct:
id d_(i1?, i2?) = g(i1, i2);

* Use the r-xi gauge for the propagators:
id Ns(i1?, i2?) = g(i1, i2)
   - (1 - xi) * (p1(i1) + p2(i1)) * (p1(i2) + p2(i2)) / s;

* Contract upper and lower indices:
repeat;
    id g(i1?, i2?L[n]) * v?(i3?U[n]) = v(i1);
    id g(i1?, i2?U[n]) * v?(i3?L[n]) = v(i1);
    id g(i1?, i2?L[n]) * g(i3?, i4?U[n]) = g(i1, i3);
    id g(i1?L[n], i2?U[n]) = 4;
    id v?(i1?L[n]) * w?(i2?U[n]) = v.w;
endrepeat;

* Simplify:
id p3 = - p1 - p1 - p4;
id v?{p4}.v?{p4} = 0;
id v?{p1, p2}.v?{p1, p2} = m^2;

id p1.p2^n? = (s/2)^n;
id p1.p4^n? = (u/2)^n;
id p2.p4^n? = (t/2)^n;

bracket T;
Print square;
.sort

* Now take the color trace (sum over inital and final state color)
id T(i1?) * T(i2?) * T(i3?) * T(i4?) = TR(i1, i2, i3, i4);
id TR(i1?, i1?, i2?, i2?) = 16/3;
id TR(i1?, i2?, i1?, i2?) = - 2/3;

bracket m;
Print square;
.end
