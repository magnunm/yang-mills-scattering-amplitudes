* Computing the spinor trace that appears when inserting
* explicit polarisations in the CoM frame for quark - antiquark
* anhilation into gluons.
* This trace gives the color ordered amplitude squared in the
* CoM frame except for a factor g^4 and a (u - m^2) or
* (t - m^2) factor.
Off statistics;
Indices mu, nu, rho, sig;
Vectors p1, p2, q1, q2, a, b, c, d, e3, e4, p3, p4;
Symbols m, s, t, u;

Global trace = ((g_(1, p2) - m) * g_(1, mu) *
              (g_(1, p1) - g_(1, q1) + m) *
              g_(1, nu) * (g_(1, p1) + m) *
              g_(1, sig) * (g_(1, p1) - g_(1, q2) + m) *
              g_(1, rho)) *
              a(mu) * b(nu) * c(rho) * d(sig);

Trace4 1;

id p1.p1 = m^2;
id p2.p2 = m^2;
id q1.q1 = 0;
id q2.q2 = 0;

.store
Vectors p1, p2, q1, q2, a, b, c, d, e3, e4, e3cc, e4cc, p3, p4, p;
Symbols m, s, t, u;

Local [A(1234)^2] = trace;

id a = e3cc;
id b = e4cc;
id c = e3;
id d = e4;
id q1 = p4;
id q2 = p4;
.sort

* Identities rely on the CoM frame
id p4.p4 = 0;
id e3cc.e4cc = 0;
id e3.e4 = 0;
id e3cc.e4 = -1;
id e4cc.e3 = -1;
id e3cc.e3 = -1;
id e4cc.e4 = -1;
id e3.p?{p3, p4} = 0;
id e3cc.p?{p3, p4} = 0;
id e4.p?{p3, p4} = 0;
id e4cc.p?{p3, p4} = 0;

.sort

* Rewriting products in terms of the Mandelstam variables
id p1.p2 = s/2 - m^2;
id p1.p3 = m^2/2 - t/2;
id p1.p4 = m^2/2 - u/2;
id p3.p4 = s/2;
id p2.p4 = m^2/2 - t/2;
id p2.p3 = m^2/2 - u/2;

id p1.e3cc = p1.e3;
id p1.e4cc = p1.e3;
id p1.e4 = p1.e3;

id p2.e3cc = p2.e3;
id p2.e4cc = p2.e3;
id p2.e4 = p2.e3;

Print [A(1234)^2];

.store
Vectors p1, p2, q1, q2, a, b, c, d, e3, e4, e3cc, e4cc, p3, p4, p;
Symbols m, s, t, u;

Local [A(1243)^2] = trace;

id a = e4cc;
id b = e3cc;
id c = e4;
id d = e3;
id q1 = p3;
id q2 = p3;
.sort

* Identities rely on the CoM frame
id p4.p4 = 0;
id e3cc.e4cc = 0;
id e3.e4 = 0;
id e3cc.e4 = -1;
id e4cc.e3 = -1;
id e3cc.e3 = -1;
id e4cc.e4 = -1;
id e3.p?{p3, p4} = 0;
id e3cc.p?{p3, p4} = 0;
id e4.p?{p3, p4} = 0;
id e4cc.p?{p3, p4} = 0;

.sort

* Rewriting products in terms of the Mandelstam variables
id p1.p2 = s/2 - m^2;
id p1.p3 = m^2/2 - t/2;
id p1.p4 = m^2/2 - u/2;
id p3.p4 = s/2;
id p2.p4 = m^2/2 - t/2;
id p2.p3 = m^2/2 - u/2;

id p1.e3cc = p1.e3;
id p1.e4cc = p1.e3;
id p1.e4 = p1.e3;

id p2.e3cc = p2.e3;
id p2.e4cc = p2.e3;
id p2.e4 = p2.e3;

Print [A(1243)^2];

.store
Vectors p1, p2, q1, q2, a, b, c, d, e3, e4, e3cc, e4cc, p3, p4, p;
Symbols m, s, t, u;

Local [A(1234)A(1243)] = trace;

id a = e3cc;
id b = e4cc;
id c = e4;
id d = e3;
id q1 = p4;
id q2 = p3;
.sort

* Identities rely on the CoM frame
id p4.p4 = 0;
id e3cc.e4cc = 0;
id e3.e4 = 0;
id e3cc.e4 = -1;
id e4cc.e3 = -1;
id e3cc.e3 = -1;
id e4cc.e4 = -1;
id e3.p?{p3, p4} = 0;
id e3cc.p?{p3, p4} = 0;
id e4.p?{p3, p4} = 0;
id e4cc.p?{p3, p4} = 0;

.sort

* Rewriting products in terms of the Mandelstam variables
id p1.p2 = s/2 - m^2;
id p1.p3 = m^2/2 - t/2;
id p1.p4 = m^2/2 - u/2;
id p3.p4 = s/2;
id p2.p4 = m^2/2 - t/2;
id p2.p3 = m^2/2 - u/2;

id p1.e3cc = p1.e3;
id p1.e4cc = p1.e3;
id p1.e4 = p1.e3;

id p2.e3cc = p2.e3;
id p2.e4cc = p2.e3;
id p2.e4 = p2.e3;

Print [A(1234)A(1243)];

.end
