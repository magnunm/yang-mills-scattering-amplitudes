Off statistics;
Indices Lmu1, Lnu1, Lrho1, Lsig1, Lal1, Lbe1,
        Umu1, Unu1, Urho1, Usig1, Ual1, Ube1,
        Lmu2, Lnu2, Lrho2, Lsig2, Lal2, Lbe2,
        Umu2, Unu2, Urho2, Usig2, Ual2, Ube2,
	i1, ..., i5, j,
	a, b, c, d, e;
Set L: Lmu1, Lnu1, Lrho1, Lsig1, Lal1, Lbe1, Lmu2, Lnu2, Lrho2, Lsig2, Lal2, Lbe2;
Set U: Umu1, Unu1, Urho1, Usig1, Ual1, Ube1, Umu2, Unu2, Urho2, Usig2, Ual2, Ube2;
Vectors p1, p2, p3, p4, N, v, w, Sv, Tv, Uv;
Tensors g(symmetric), E, Ec, f(antisymmetric),
        Ns(symmetric), TR(cyclic);
* Needs to be non-commuting here because we supress fundamental rep indices.
NTensors T;
* sU, sV stands for the spinors u and v.
Functions sU, sUbar, sV, sVbar;
Symbols s, t, u, xi, n, m, i, [(t - m^2)], [(u - m^2)], alphaS, pi;

* The amplitude divided by g^2:

Local Ms = -i * (T(c) * T(d) - T(d) * T(c))
      	   * Ns(Lal1, Lbe1) *
      	   (g(Urho1, Usig1) * (p3(Ube1) - p4(Ube1))
	   + g(Usig1, Ube1) * (p4(Urho1) - Sv(Urho1))
	   + g(Ube1, Urho1) * (Sv(Usig1) - p3(Usig1))) *
	   E(Lrho1, Lsig1) *
           sVbar(p2) * g_(1, Ual1) * sU(p1);

Local Mu = -i * T(c) * T(d) * E(Lrho1, Lsig1) *
           sVbar(p2) *
           g_(1, Urho1) * (g_(1, Uv) + m) * g_(1, Usig1) *
           sU(p1);

Local Mt = -i * T(d) * T(c) * E(Lrho1, Lsig1) *
           sVbar(p2) *
           g_(1, Usig1) * (g_(1, Tv) + m) * g_(1, Urho1) *
           sU(p1);

* In the complex conjugate amplitude the order of the color factors
* are reversed.  Also choose the contraction indices such that we get
* one upper and one lower index in the trace.

Local MsCC = i * (T(d) * T(c) - T(c) * T(d))
      	   * Ns(Ual2, Ube2) *
      	   (g(Lrho2, Lsig2) * (p3(Lbe2) - p4(Lbe2))
	   + g(Lsig2, Lbe2) * (p4(Lrho2) - Sv(Lrho2))
	   + g(Lbe2, Lrho2) * (Sv(Lsig2) - p3(Lsig2))) *
	   Ec(Urho2, Usig2) *
           sUbar(p1) * g_(1, Lal2) * sV(p2);

Local MuCC = i * T(d) * T(c) * Ec(Urho2, Usig2) *
             sUbar(p1) *
             g_(1, Lsig2) * (g_(1, Uv) + m) * g_(1, Lrho2) *
             sV(p2);

Local MtCC = i * T(c) * T(d) * Ec(Urho2, Usig2) *
             sUbar(p1) *
             g_(1, Lrho2) * (g_(1, Tv) + m) * g_(1, Lsig2) *
             sV(p2);

* Replace placeholder 4-vectors
id Sv = p1 + p2;
id Tv = p1 + p3;
id Uv = p1 + p4;

.sort

* The square:
Local square = (Ms/s + Mu/[(u - m^2)] + Mt/[(t - m^2)]) *
               (MsCC/s + MuCC/[(u - m^2)] + MtCC/[(t - m^2)]);

id i^2 = -1;

* Sum over spins
id sU(w?) * sUbar(w?) = g_(1, w) + m;
id sV(w?) * sVbar(w?) = g_(1, w) - m;

* Since we are sure that the expressions are now surrounded by
* spinors like uBar(?) * (...) * u(?)  with no internal spinors in
* (...) we can be sure the following works correctly:
id sUbar(w?) = g_(1, w) + m;
id sU(w?) = 1;
id sVbar(w?) = g_(1, w) - m;
id sV(w?) = 1;

* Take the trace:
Trace4 1;

* We chose the indices so that this is correct:
id d_(i1?, i2?) = g(i1, i2);

* Choose some gauge for the propagators:
* R_xi gauge
id Ns(i1?, i2?) = g(i1, i2) -
   (1 - xi) * (p1(i1) + p2(i1)) * (p1(i2) + p2(i2)) / s;

* And insert for the polarisation sum the covariant
* expression, only valid as long as we use ghosts to
* cancel the additional introduced degrees of freedom.
id E(i1?, i2?) * Ec(i3?, i4?) = g(i1, i3) * g(i2, i4);

* Contract upper and lower indices:
repeat;
    id g(i1?, i2?L[n]) * v?(i3?U[n]) = v(i1);
    id g(i1?, i2?U[n]) * v?(i3?L[n]) = v(i1);
    id g(i1?, i2?L[n]) * g(i3?, i4?U[n]) = g(i1, i3);
    id g(i1?L[n], i2?U[n]) = 4;
    id v?(i1?L[n]) * w?(i2?U[n]) = v.w;
endrepeat;

* Now take the color trace (sum over final state color)
id T(i1?) * T(i2?) * T(i3?) * T(i4?) = TR(i1, i2, i3, i4);
id TR(i1?, i1?, i2?, i2?) = 16/3;
id TR(i1?, i2?, i1?, i2?) = - 2/3;

* Simplify
id p3 = - p1 - p2 - p4;
id v?{p4}.v?{p4} = 0;
id v?{p1, p2}.v?{p1, p2} = m^2;

id p1.p2^n? = (s/2)^n;
id p1.p4^n? = (u/2)^n;
id p2.p4^n? = (t/2)^n;

Print square;
.sort

* The m=0 case:
Local squareNoM = square;

id m = 0;
id [(u - m^2)]^n? = u^n;
id [(t - m^2)]^n? = t^n;

Print squareNoM;
.end
