"""
Use crossing symmetry to obtain the correct result for the gluon-gluon
scattering amplitude from the four gluon, two ghost and four ghost
terms computed replacing the sum over polarizations by eta^(mu nu).
These are computed in the FORM script "4-feynman-gauge.frm" in this directory.
Using the crossing here we need only compute 3 cross terms in that FORM file,
even though there are in total 19 cross terms/squares that need to be
considered when replacing eta^(mu nu) on all external lines.
"""
import importlib.util
import os

import sympy as sy

# Relative import of ../utilities/simplify_mandelstam.py
project_root = os.path.split(os.path.dirname(os.path.realpath(__file__)))[0]
spec = importlib.util.spec_from_file_location(
    "utilities.simplify_mandelstam",
    f"{project_root}/utilities/simplify_mandelstam.py"
)
simplify_mandelstam = importlib.util.module_from_spec(spec)
spec.loader.exec_module(simplify_mandelstam)

# Gluon and ghost amplitudes.
# These expression are copy-pasted directly from the output of the FORM script
# "4-feynman-gauge.frm" in this directory.
# Notation:
#    GL = Gluon summed over polarization with eta^(mu nu)
#    UI = Ghost with spin line parallell to momentum
#    UO = Ghost with spin line oposite momentum
s, t, u = sy.symbols("s t u")

gl1_gl2_gl3_gl4 = sy.sympify("""
6048 - 1800*s^-2*t*u + 720*s^-1*u + 720*s^-1*t + 720*t^-1*u + 720*t*u^-1
- 1800*s*t^-2*u + 720*s*t^-1 + 720*s*u^-1 - 1800*s*t*u^-2
""")

gl1_gl2_ui3_uo4_cross = sy.sympify("""
72 + 180*s^-2*t*u + 180*s^-1*u + 180*s^-1*t - 36*t^-1*u - 36*t*u^-1 - 36
*s*t^-1 - 36*s*u^-1
""")

ui1_uo2_ui3_uo4_cross = sy.sympify("""
- 36 - 18*s^-2*t*u - 18*s^-1*u - 18*s^-1*t - 18*t*u^-1 - 18*s*u^-1 - 18
*s*t*u^-2
""")

# With all external gluons in the Feynman-t' Hooft gauge crossing symmetry
# holds. This we can use to get all the relevant 2 and 4 ghost amplitudes from
# the two above!
all_crosssings_two_ghosts = simplify_mandelstam.cross_into_all_channels(
    gl1_gl2_ui3_uo4_cross, s, t, u
)

all_crosssings_four_ghosts = simplify_mandelstam.cross_into_all_channels(
    ui1_uo2_ui3_uo4_cross, s, t, u
)

two_ghost_terms = 2 * (  # Factor 2 for complex conjugate terms
    2 * gl1_gl2_ui3_uo4_cross  # gl1_gl2_ui3_uo4_cross + ui1_uo2_gl3_gl4_cross
    + 2 * all_crosssings_two_ghosts['tsu']  # gl1_ui2_gl3_uo4 + ui1_gl2_uo3_gl4
    + 2 * all_crosssings_two_ghosts['ust']  # gl1_ui2_uo3_gl4 + ui1_gl2_gl3_uo4
)

four_ghost_terms = 2 * (  # Factor 2 for complex conjugate terms
    ui1_uo2_ui3_uo4_cross
    + all_crosssings_four_ghosts['sut']  # ui1_uo2_uo3_ui4_cross
    + all_crosssings_four_ghosts['ust']  # ui1_ui2_uo3_uo4_cross
)

four_gluon_amp = (
    gl1_gl2_gl3_gl4 + two_ghost_terms + four_ghost_terms
)

print("""
The sum of gluon and relevant ghost terms using the Feynman t' Hooft gauge for
all external particles:
""")
sy.pprint(
    simplify_mandelstam.simplify_mandelstam(
        four_gluon_amp,
        s, t, u
    )
)
print("Result obtained via crossing!")
