Off statistics;
* Computing the tree level amplitude for gg -> gg using the Feynman
* gauge for two gluon polarization sums (p3 and p4) and the light-cone gauge
* for the two other. This requires including also the three diagrams with two
* external ghosts at the p3, p4 positions.
* Uses R_xi gauge for internal propagators as a consistency check that the
* xi does not appear in the final expression. Using the Feynman gauge
* gives considerably shorter runtime.
Indices Lmu1, Lnu1, Lrho1, Lsig1, Lal1, Lbe1,
        Umu1, Unu1, Urho1, Usig1, Ual1, Ube1,
        Lmu2, Lnu2, Lrho2, Lsig2, Lal2, Lbe2,
        Umu2, Unu2, Urho2, Usig2, Ual2, Ube2,
        a, b, c, d, e,
	i1, ..., i10, i, j;
Set L: Lmu1, Lnu1, Lrho1, Lsig1, Lal1, Lbe1, Lmu2, Lnu2, Lrho2, Lsig2, Lal2, Lbe2;
Set U: Umu1, Unu1, Urho1, Usig1, Ual1, Ube1, Umu2, Unu2, Urho2, Usig2, Ual2, Ube2;
Vectors p1, p2, p3, p4, Sv, Tv, Uv, n1, n2, v, w;
Tensors g(symmetric), f(antisymmetric),
        polsum1, polsum2, polsum3, polsum4,
        Ns(symmetric), Nu(symmetric), Nt(symmetric), Tr(cyclic);
Symbols s, t, u, n, xi;

* Components of the 4-gluon amplitude
* Diveded by -g^2

Local M4 = f(a, b, e) * f(c, d, e) *
      	   (g(Umu1, Urho1) * g(Unu1, Usig1) - g(Umu1, Usig1) * g(Unu1, Urho1))
          +f(a, c, e) * f(d, b, e) *
	  (g(Umu1, Usig1) * g(Urho1, Unu1) - g(Umu1, Unu1) * g(Urho1, Usig1))
          +f(a, d, e) * f(b, c, e) *
	  (g(Umu1, Unu1) * g(Urho1, Usig1) - g(Umu1, Urho1) * g(Unu1, Usig1));

Local Ms = f(a, b, e) * f(c, d, e) *
          (g(Umu1, Unu1)*(p1(Ual1) - p2(Ual1))
	  + g(Unu1, Ual1)*(p2(Umu1) + Sv(Umu1))
	  - g(Ual1, Umu1)*(Sv(Unu1) + p1(Unu1)))
          * Ns(Lal1, Lbe1) *
          (g(Urho1, Usig1)*(p3(Ube1) - p4(Ube1))
	  + g(Usig1, Ube1)*(p4(Urho1) - Sv(Urho1))
	  + g(Ube1, Urho1)*(Sv(Usig1) - p3(Usig1)));

Local Mu = f(d, a, e) * f(b, c, e) *
          (g(Usig1, Umu1)*(p4(Ual1) - p1(Ual1))
	  + g(Umu1, Ual1)*(p1(Usig1) + Uv(Usig1))
	  - g(Ual1, Usig1)*(Uv(Umu1) + p4(Umu1)))
          * Nu(Lal1, Lbe1) *
          (g(Unu1, Urho1)*(p2(Ube1) - p3(Ube1))
	  + g(Urho1, Ube1)*(p3(Unu1) - Uv(Unu1))
	  + g(Ube1, Unu1)*(Uv(Urho1) - p2(Urho1)));

Local Mt = f(c, a, e) * f(b, d, e) *
          (g(Urho1, Umu1)*(p3(Ual1) - p1(Ual1)) +
	  g(Umu1, Ual1)*(p1(Urho1) + Tv(Urho1)) -
	  g(Ual1, Urho1)*(Tv(Umu1) + p3(Umu1)))
          * Nt(Lal1, Lbe1) *
          (g(Unu1, Usig1)*(p2(Ube1) - p4(Ube1))
	  + g(Usig1, Ube1)*(p4(Unu1) - Tv(Unu1))
	  + g(Ube1, Unu1)*(Tv(Usig1) - p2(Usig1)));

Local M4CC = f(a, b, e) * f(c, d, e) *
      	   (g(Umu2, Urho2) * g(Unu2, Usig2) - g(Umu2, Usig2) * g(Unu2, Urho2))
          +f(a, c, e) * f(d, b, e) *
	  (g(Umu2, Usig2) * g(Urho2, Unu2) - g(Umu2, Unu2) * g(Urho2, Usig2))
          +f(a, d, e) * f(b, c, e) *
	  (g(Umu2, Unu2) * g(Urho2, Usig2) - g(Umu2, Urho2) * g(Unu2, Usig2));

Local MsCC = f(a, b, e) * f(c, d, e) *
          (g(Umu2, Unu2)*(p1(Ual2) - p2(Ual2))
	  + g(Unu2, Ual2)*(p2(Umu2) + Sv(Umu2))
	  - g(Ual2, Umu2)*(Sv(Unu2) + p1(Unu2)))
          * Ns(Lal2, Lbe2) *
          (g(Urho2, Usig2)*(p3(Ube2) - p4(Ube2))
	  + g(Usig2, Ube2)*(p4(Urho2) - Sv(Urho2))
	  + g(Ube2, Urho2)*(Sv(Usig2) - p3(Usig2)));

Local MuCC = f(d, a, e) * f(b, c, e) *
          (g(Usig2, Umu2)*(p4(Ual2) - p1(Ual2))
	  + g(Umu2, Ual2)*(p1(Usig2) + Uv(Usig2))
	  - g(Ual2, Usig2)*(Uv(Umu2) + p4(Umu2)))
          * Nu(Lal2, Lbe2) *
          (g(Unu2, Urho2)*(p2(Ube2) - p3(Ube2))
	  + g(Urho2, Ube2)*(p3(Unu2) - Uv(Unu2))
	  + g(Ube2, Unu2)*(Uv(Urho2) - p2(Urho2)));

Local MtCC = f(c, a, e) * f(b, d, e) *
          (g(Urho2, Umu2)*(p3(Ual2) - p1(Ual2)) +
	  g(Umu2, Ual2)*(p1(Urho2) + Tv(Urho2)) -
	  g(Ual2, Urho2)*(Tv(Umu2) + p3(Umu2)))
          * Nt(Lal2, Lbe2) *
          (g(Unu2, Usig2)*(p2(Ube2) - p4(Ube2))
	  + g(Usig2, Ube2)*(p4(Unu2) - Tv(Unu2))
	  + g(Ube2, Unu2)*(Tv(Usig2) - p2(Usig2)));

* Choose some gauge for the propagators:
**** R_xi gauge
**** NB! This choice increases runtime by about a factor of 10 compared to
**** Feynman gauge. The result is the same (as it should be) in both cases.
id Ns(i1?, i2?) = g(i1, i2);* - (1 - xi) * Sv(i1) * Sv(i2) / s;
id Nu(i1?, i2?) = g(i1, i2);* - (1 - xi) * Uv(i1) * Uv(i2) / u;
id Nt(i1?, i2?) = g(i1, i2);* - (1 - xi) * Tv(i1) * Tv(i2) / t;

* Replace the placeholder 4-vectors
id Sv = p1 + p2;
id Uv = p1 + p4;
id Tv = p1 + p3;

.sort
Local amp = (M4 + Ms/s + Mu/u + Mt/t);

Local ampCC = (M4CC + MsCC/s + MuCC/u + MtCC/t);

* Expand the color factors:
id f(a?, b?, e?) * f(c?, d?, e?) = 2*(-Tr(a, b, c, d)
   	     	   	     	   + Tr(a, b, d, c)
				   + Tr(a, c, d, b)
				   - Tr(a, d, c, b));

* Contract upper and lower indices:
repeat;
    id g(i1?, i2?L[n]) * v?(i3?U[n]) = v(i1);
    id g(i1?, i2?U[n]) * v?(i3?L[n]) = v(i1);
    id g(i1?, i2?L[n]) * g(i3?, i4?U[n]) = g(i1, i3);
    id g(i1?L[n], i2?U[n]) = 4;
    id v?(i1?L[n]) * w?(i2?U[n]) = v.w;
endrepeat;

* Lightlike 4 -vectors:
id v?{p1, p2, p3, p4}.v?{p1, p2, p3, p4} = 0;

.sort
* Square and sum over color:
* Now also add the polarization vectors summed over helicity as general
* "polsum" tensors.
Global squareGluon = amp * ampCC *
                polsum1(Lmu1, Lmu2) * polsum2(Lnu1, Lnu2) *
                polsum3(Lrho1, Lrho2) * polsum4(Lsig1, Lsig2);

id Tr(i1?, i2?, i3?, i4?) * Tr(i4?, i3?, i2?, i1?) = 19/6;
id Tr(i1?, i2?, i3?, i4?) * Tr(i4?, i3?, i1?, i2?) = -1/3;
id Tr(i1?, i2?, i3?, i4?) * Tr(i4?, i1?, i2?, i3?) = 2/3;

* For p3 and p4 we use the Feynman-t' Hooft gauge
id polsum4(i1?, i2?) = - g(i1, i2);
id polsum3(i1?, i2?) = - g(i1, i2);

* Contract upper and lower indices:
repeat;
    id g(i1?, i2?L[n]) * v?(i3?U[n]) = v(i1);
    id g(i1?, i2?U[n]) * v?(i3?L[n]) = v(i1);
    id g(i1?, i2?L[n]) * g(i3?, i4?U[n]) = g(i1, i3);
    id g(i1?L[n], i2?U[n]) = 4;
    id v?(i1?L[n]) * w?(i2?U[n]) = v.w;
endrepeat;

.sort
* Insert the light cone gauge expressions for the remaining pol sums.
id polsum1(i1?, i2?) = - g(i1, i2) + (p1(i1)*n1(i2) + n1(i1)*p1(i2))/(p1.n1);
id polsum2(i1?, i2?) = - g(i1, i2) + (p2(i1)*n2(i2) + n2(i1)*p2(i2))/(p2.n2);

* Contract upper and lower indices:
repeat;
    id g(i1?, i2?L[n]) * v?(i3?U[n]) = v(i1);
    id g(i1?, i2?U[n]) * v?(i3?L[n]) = v(i1);
    id g(i1?, i2?L[n]) * g(i3?, i4?U[n]) = g(i1, i3);
    id g(i1?L[n], i2?U[n]) = 4;
    id v?(i1?L[n]) * w?(i2?U[n]) = v.w;
endrepeat;

* Lightlike 4-vectors
id v?{p4, p3, p1, p2}.v?{p4, p3, p1, p2} = 0;

.store
Off statistics;
Indices Lmu1, Lnu1, Lrho1, Lsig1, Lal1, Lbe1,
        Umu1, Unu1, Urho1, Usig1, Ual1, Ube1,
        Lmu2, Lnu2, Lrho2, Lsig2, Lal2, Lbe2,
        Umu2, Unu2, Urho2, Usig2, Ual2, Ube2,
        a, b, c, d, e,
	i1, ..., i10;
Set L: Lmu1, Lnu1, Lrho1, Lsig1, Lal1, Lbe1, Lmu2, Lnu2, Lrho2, Lsig2, Lal2, Lbe2;
Set U: Umu1, Unu1, Urho1, Usig1, Ual1, Ube1, Umu2, Unu2, Urho2, Usig2, Ual2, Ube2;
Vectors p1, p2, p3, p4, v, w, n1, n2, Sv;
Tensors g(symmetric), f(antisymmetric), Tr(cyclic), N, polsum1, polsum2;
Symbols s, t, u, n, xi;

* The two ghost and two gluon amplitude, except a factor g^2.
* Here p4 is the ghost with the spin line incoming.
* Polarisation vectors added later.

Local twoGhostAmp =
      - f(a, b, e) * f(d, e, c) * N(Lal1, Lbe1) * p3(Ual1) * (
             g(Umu1, Unu1)*(p1(Ube1) - p2(Ube1)) +
	     g(Unu1, Ube1)*(p2(Umu1) + Sv(Umu1)) -
	     g(Ube1, Umu1)*(Sv(Unu1) + p1(Unu1))
             ) / (2*p4.p3)
      + f(d, a, e) * f(b, c, e) * (p4(Umu1) + p1(Umu1)) * p3(Unu1) / (2*p4.p1)
      + f(c, a, e) * f(b, d, e) * (p4(Unu1) + p2(Unu1)) * p3(Umu1) / (2*p4.p2);

* A special thing about the ghosts: It is the cross-term between the two
* amplitudes of different spin lines that cancels the longitudinal
* contributions to the gluon amplitude! So here we have the ghost amp
* with the p3 spin line incoming.
* Complex conjugation reverses the order of the free color indices due to the
* generators T^a being hermitian.

Local twoGhostAmpCC =
      - f(b, a, e) * f(c, e, d) * N(Lal2, Lbe2) * p4(Ual2) * (
             g(Umu2, Unu2)*(p1(Ube2) - p2(Ube2)) +
	     g(Unu2, Ube2)*(p2(Umu2) + Sv(Umu2)) -
	     g(Ube2, Umu2)*(Sv(Unu2) + p1(Unu2))
             ) / (2*p4.p3)
      + f(a, c, e) * f(b, d, e) * (p3(Umu2) + p1(Umu2)) * p4(Unu2) / (2*p3.p1)
      + f(a, d, e) * f(b, c, e) * (p3(Unu2) + p2(Unu2)) * p4(Umu2) / (2*p3.p2);


* R_xi gauge for the gluon propagator
id N(i1?, i2?) = g(i1, i2);* - (1 - xi) * Sv(i1) * Sv(i2) / s;

id Sv = p1 + p2;

* Expand the color factors:
id f(a?, b?, e?) * f(c?, d?, e?) = 2*(-Tr(a, b, c, d)
   	     	   	     	   + Tr(a, b, d, c)
				   + Tr(a, c, d, b)
				   - Tr(a, d, c, b));

.sort
* Square and add the
* sum over polarisation vectors.
Global ghostCrossTerm = twoGhostAmp * twoGhostAmpCC *
       polsum1(Lmu1, Lmu2) * polsum2(Lnu1, Lnu2);

* Insert the light cone gauge expressions for the pol sums.
id polsum1(i1?, i2?) = - g(i1, i2) + (p1(i1)*n1(i2) + n1(i1)*p1(i2))/(p1.n1);
id polsum2(i1?, i2?) = - g(i1, i2) + (p2(i1)*n2(i2) + n2(i1)*p2(i2))/(p2.n2);

* Sum over color.
id Tr(i1?, i2?, i3?, i4?) * Tr(i4?, i3?, i2?, i1?) = 19/6;
id Tr(i1?, i2?, i3?, i4?) * Tr(i4?, i3?, i1?, i2?) = -1/3;
id Tr(i1?, i2?, i3?, i4?) * Tr(i4?, i1?, i2?, i3?) = 2/3;

* Contract upper and lower indices:
repeat;
    id g(i1?, i2?L[n]) * v?(i3?U[n]) = v(i1);
    id g(i1?, i2?U[n]) * v?(i3?L[n]) = v(i1);
    id g(i1?, i2?L[n]) * g(i3?, i4?U[n]) = g(i1, i3);
    id g(i1?L[n], i2?U[n]) = 4;
    id v?(i1?L[n]) * w?(i2?U[n]) = v.w;
endrepeat;

* Lightlike 4-vectors
id v?{p4, p3, p1, p2}.v?{p4, p3, p1, p2} = 0;

.store
Off statistics;
Vectors p1, p2, p3, p4, n1, n2, v, w;
Symbols s, t, u, n, E;

Local squareGluonContribution = squareGluon;
Local ghostContribution = ghostCrossTerm;

* Identities valid for n1,n2 in the CoM frame.
* Introducing the CoM energy E = E_1 = sqrt(s)/2 is helpful
id n1.p2 = 0;
id n2.p1 = 0;
id p1.n1^n? = (2*E)^n;
id p2.n2^n? = (2*E)^n;
id p3.n2 = (p3.p1)/E;
id p3.n1 = (p3.p2)/E;
id p4.n2 = (p4.p1)/E;
id p4.n1 = (p4.p2)/E;
id n1.n2 = (p2.p1)/(E^2);

.sort
* Rewrite in Mandelstam variables
id p1.p2^n? = (s/2)^n;
id p3.p4^n? = (s/2)^n;
id p1.p4^n? = (u/2)^n;
id p2.p3^n? = (u/2)^n;
id p1.p3^n? = (t/2)^n;
id p2.p4^n? = (t/2)^n;
id E^n? = (s/4)^(n/2);

.sort
* Simplify Mandelstam expression

* Remove large powers
repeat;
        id u^5 = - (s + t) * u^4;
        id u^4 = - (s + t) * u^3;
        id u^3 = - (s + t) * u^2;
        id t^4 = - (s + u) * t^3;
        id t^3 = - (s + u) * t^2;
endrepeat;

* Some Mandelstam relations (the order is important).
id t*u^2/s^3 = - t*u/s^2 - t^2*u/s^3;
id u^2/(s*t) = 2 + s/t + t/s;
id t^2/(s*u) = 2 + s/u + u/s;
id s^2/(t*u) = 2 + t/u + u/t;
id t^2/s^2 = -t/s - t*u/s^2;
id u^2/s^2 = - u/s - t*u/s^2;
id t^2/u^2 = - t/u - t*s/u^2;
id s^2/u^2 = - s/u - t*s/u^2;
id s^2/t^2 = - s/t - s*u/t^2;
id u^2/t^2 = - u/t - s*u/t^2;

Print squareGluonContribution, ghostContribution;
.end
