(* ::Package:: *)

(* ::Title:: *)
(*Gluon - gluon scattering *)


(* ::Subtitle:: *)
(*Cancelling longitudinal polarizations with ghosts*)


(* ::Section:: *)
(*General function for squared amplitudes*)


$LoadAddOns = {"FeynArts"};
Needs["FeynCalc`"]


(* ::Text:: *)
(*First we define a function to generate all the relevant diagrams. Including all the relevant ghost diagrams.*)


diag[in_List, out_List]:=
  InsertFields[CreateTopologies[0, 2->2], in->out,
               InsertionLevel->{Classes}, Model->"SMQCD"]


amp[in_List, out_List]:=
  FCFAConvert[CreateFeynAmp[diag[in, out]],
              IncomingMomenta->{p1, p2}, OutgoingMomenta->{p3, p4},
              UndoChiralSplittings->True, ChangeDimension->4,
              TransversePolarizationVectors->{}, List->False,
              SMP->True, Contract->True, DropSumOver->True]


FCClearScalarProducts[];
SetMandelstam[s, t, u, p1, p2, -p3, -p4, 0, 0, 0, 0];


(* ::Text:: *)
(*A general function to compute the cross term between two amplitudes. Be aware that it is somewhat slow, especially with 4 gluons. For convenience it is memoized, a change in its method of working therefore requires a restart of the kernel to take effect in already computed values.*)


numGhosts[fields_List] :=
  (* Helper function that counts the number of  QCD ghosts in a list of FeynArts fields. *)
  Count[fields, U[5]] + Count[fields, -U[5]]


crossTerm[in1_List, out1_List, in2_List, out2_List, polsumVectors_List]:=
  (* Compute the cross term between the amplitude (`in1` -> `out1`) and (`in2` -> `out2`). 
  The `in` and `out` lists are in the format of FeynArts fields.
  `polsumVectors` is passed to `DoPolarizationSums` for the four external lines. *)
  crossTerm[in1, out1, in2, out2, polsumVectors]=
  (TrickMandelstam[#, {s, t, u, 0}] & )[
    (DoPolarizationSums[#, p4, polsumVectors[[4]]] & )[
      (DoPolarizationSums[#, p3, polsumVectors[[3]]] & )[
        (DoPolarizationSums[#, p2, polsumVectors[[2]]] & )[
          (DoPolarizationSums[#, p1, polsumVectors[[1]]] & )[
            ReplaceAll[SUNN -> 3][
              (SUNSimplify[#, Explicit -> True, SUNNToCACF -> False] & )[
                FeynAmpDenominatorExplicit[
                  (amp[in1,out1] * FeynCalc`ComplexConjugate[amp[in2, out2]])
                ]]]]]]]] /
  (* In the case some external line does not contain a gluon to sum over polarization this instead has 
  the effect of multiplying the result by a factor 2. 
  We must correct by a factor 2^(number of ghosts) therefore. *)
  (Power[2, Min[numGhosts[Join[in1, out1]], numGhosts[Join[in2, out2]]]])


square[in_List, out_List, polsumVectors_List]:=
  (* Lenght squared of the amplitude for (`in` \[Rule] `out`) *)
  crossTerm[in, out, in, out, polsumVectors]


(* ::Section:: *)
(*Example calculations*)


(* ::Text:: *)
(*Notation used in the names:*)
(*UI = ghost spin line in*)
(*UO = ghost spin line out*)
(*GT = transverse gluon*)
(*GL = non - transverse gluon*)


knownResult = 1152 * FeynCalc`SMP["g_s"]^4 *
              (3 - (t * u)/s^2 - (s * u)/t^2 - (s * t)/u^2)


(* ::Subsection:: *)
(*All physical*)


GT1GT2GT3GT4 = square[{V[5], V[5]}, {V[5], V[5]}, {p2, p1, p4, p3}]


FeynCalc`FCCompareResults[
  GT1GT2GT3GT4,
  knownResult,
  Factoring->Function[x, Simplify[TrickMandelstam[x, {s, t, u, 0}]]],
  Text->{"All physical polarizations:", "CORRECT.", "WRONG!"}];


(* ::Subsection:: *)
(*Two unphysical*)


GT1GT2GL3GL4 = square[{V[5], V[5]}, {V[5], V[5]}, {p2, p1, 0, 0}]


GT1GT2UI3UO4 = square[{V[5], V[5]}, {-U[5], U[5]}, {p2, p1, 0, 0}]


GT1GT2UI3UO4crossGT1GT2UO3UI4 = crossTerm[{V[5], V[5]}, {-U[5], U[5]},
                                          {V[5], V[5]}, {U[5], -U[5]},
                                          {p2, p1, 0, 0}]


(* ::Text:: *)
(*With only two unphysical polarizations it does not matter if we use the square of each ghost diagram or the cross term between the two different spin lines, as we demonstrate here. For more unphysical polarizations however, the cross term must be used.*)


FeynCalc`FCCompareResults[
  GT1GT2GL3GL4 - 2 * GT1GT2UI3UO4,
  knownResult,
  Factoring->Function[x, Simplify[TrickMandelstam[x, {s, t, u, 0}]]],
  Text->{"Unphysical at 3,4 cancelled by subtracting two times square ghosts:", "CORRECT.", "WRONG!"}];


FeynCalc`FCCompareResults[
  GT1GT2GL3GL4 + 2 * GT1GT2UI3UO4crossGT1GT2UO3UI4,
  knownResult,
  Factoring->Function[x, Simplify[TrickMandelstam[x, {s, t, u, 0}]]],
  Text->{"Unphysical at 3,4 cancelled by adding two times ghost cross term:", "CORRECT.", "WRONG!"}];


GT1GL2GT3GL4 = square[{V[5], V[5]}, {V[5], V[5]}, {p2, 0, p4, 0}]


GT1UI2GT3UO4 = square[{V[5], U[5]}, {V[5], U[5]}, {p2, 0, p4, 0}]


GT1UI2GT3UO4crossGT1UO2GT3UI4 = crossTerm[{V[5], U[5]}, {V[5], U[5]},
                                          {V[5], -U[5]}, {V[5], -U[5]},
                                          {p2, 0, p4, 0}]


FeynCalc`FCCompareResults[
  GT1GL2GT3GL4 - 2 * GT1UI2GT3UO4,
  knownResult,
  Factoring->Function[x, Simplify[TrickMandelstam[x, {s, t, u, 0}]]],
  Text->{"Unphysical at 2,4 cancelled by subtracting two times square ghosts:", "CORRECT.", "WRONG!"}];


FeynCalc`FCCompareResults[
  GT1GL2GT3GL4 + 2 * GT1UI2GT3UO4crossGT1UO2GT3UI4,
  knownResult,
  Factoring->Function[x, Simplify[TrickMandelstam[x, {s, t, u, 0}]]],
  Text->{"Unphysical at 2,4 cancelled by adding two times ghost cross term:", "CORRECT.", "WRONG!"}];


(* ::Subsection:: *)
(*Three unphysical*)


(* ::Text:: *)
(*Now the cross terms and squares are no longer equal for the ghosts, so it is important that we use the cross terms.*)


GT1GL2GL3GL4 = square[{V[5], V[5]}, {V[5], V[5]}, {p2, 0, 0, 0}]


GT1GL2UI3UO4crossGT1GL2UO3UI4 = crossTerm[{V[5], V[5]}, {-U[5], U[5]},
                                          {V[5], V[5]}, {U[5], -U[5]},
                                          {p2, 0, 0, 0}]


GT1UI2GL3UO4crossGT1UO2GL3UI4 = crossTerm[{V[5], U[5]}, {V[5], U[5]},
                                          {V[5], -U[5]}, {V[5], -U[5]},
                                          {p2, 0, 0, 0}]


GT1UI2UO3GL4crossGT1UO2UI3GL4 = crossTerm[{V[5], U[5]}, {U[5], V[5]},
                                          {V[5], -U[5]}, {-U[5], V[5]},
                                          {p2, 0, 0, 0}]


FeynCalc`FCCompareResults[
  GT1GL2GL3GL4
  + 2 * GT1GL2UI3UO4crossGT1GL2UO3UI4
  + 2 * GT1UI2GL3UO4crossGT1UO2GL3UI4
  + 2 * GT1UI2UO3GL4crossGT1UO2UI3GL4,
  knownResult,
  Factoring->Function[x, Simplify[TrickMandelstam[x, {s, t, u, 0}]]],
  Text->{"Unphysical at 2, 3 and 4 cancelled by adding the six possible ghost cross terms:", "CORRECT.", "WRONG!"}];


(* ::Text:: *)
(*Just to show the squares are  no longer equal to the cross terms: (this would have been equal to GT1GL2UI3UO4crossGT1GL2UO3UI4 in that case)*)


GT1GL2UI3UO4 = square[{V[5], V[5]}, {-U[5], U[5]}, {p2, 0, 0, 0}]


(* ::Subsection:: *)
(*All unphysical*)


GL1GL2GL3GL4 = square[{V[5], V[5]}, {V[5], V[5]}, {0, 0, 0, 0}]


(* ::Text:: *)
(*As before it is crucial that we use the ghost cross terms now.*)


(* ::Text:: *)
(*The same ghost amplitudes as in the three unphysical case, only now \[Epsilon](1) is not transverse.*)


GL1GL2UI3UO4crossGL1GL2UO3UI4 = crossTerm[{V[5], V[5]}, {-U[5], U[5]},
                                          {V[5], V[5]}, {U[5], -U[5]},
                                          {0, 0, 0, 0}]


GL1UI2GL3UO4crossGL1UO2GL3UI4 = crossTerm[{V[5], U[5]}, {V[5], U[5]},
                                          {V[5], -U[5]}, {V[5], -U[5]},
                                          {0, 0, 0, 0}]


GL1UI2UO3GL4crossGL1UO2UI3GL4 = crossTerm[{V[5], U[5]}, {U[5], V[5]},
                                          {V[5], -U[5]}, {-U[5], V[5]},
                                          {0, 0, 0, 0}]


(* ::Text:: *)
(*Now we need also the 3 cross terms where there is a ghost at p_1.*)
(*Notice that these have equal values as the ones above: this we could have seen from the fact that they are given by relabelings of momenta.*)


UI1GL2GL3UO4crossUO1GL2GL3UI4 = crossTerm[{U[5], V[5]}, {V[5], U[5]},
                                          {-U[5], V[5]}, {V[5], -U[5]},
                                          {0, 0, 0, 0}]


UI1GL2UO3GL4crossUO1GL2UI3GL4 = crossTerm[{U[5], V[5]}, {U[5], V[5]},
                                          {-U[5], V[5]}, {-U[5], V[5]},
                                          {0, 0, 0, 0}]


UI1UO2GL3GL4crossUO1UI2GL3GL4 = crossTerm[{U[5], -U[5]}, {V[5], V[5]},
                                          {-U[5], U[5]}, {V[5], V[5]},
                                          {0, 0, 0, 0}]


(* ::Text:: *)
(*Finally we now need to include also the process with 4 external ghosts.*)


UI1UO2UI3UO4cross = crossTerm[{U[5], -U[5]}, {-U[5], U[5]},
                              {-U[5], U[5]}, {U[5], -U[5]},
                              {0, 0, 0, 0}]


UI1UO2UO3UI4cross = crossTerm[{U[5], -U[5]}, {U[5], -U[5]},
                              {-U[5], U[5]}, {-U[5], U[5]},
                              {0, 0, 0, 0}]


UI1UI2UO3UO4cross = crossTerm[{U[5],  U[5]},  {U[5], U[5]},
                              {-U[5],  -U[5]},  {-U[5], -U[5]},
                              {0, 0, 0, 0}]


(* ::Text:: *)
(*The three other four ghost cross terms we see are just complex conjugates of the  above.*)


FeynCalc`FCCompareResults[
  GL1GL2GL3GL4
  + 2 * GL1GL2UI3UO4crossGL1GL2UO3UI4
  + 2 * GL1UI2GL3UO4crossGL1UO2GL3UI4
  + 2 * GL1UI2UO3GL4crossGL1UO2UI3GL4
  + 2 * UI1GL2GL3UO4crossUO1GL2GL3UI4
  + 2 * UI1GL2UO3GL4crossUO1GL2UI3GL4
  + 2 * UI1UO2GL3GL4crossUO1UI2GL3GL4
  + 2 * UI1UO2UI3UO4cross
  + 2 * UI1UO2UO3UI4cross
  + 2 * UI1UI2UO3UO4cross,
  knownResult,
  Factoring->Function[x, Simplify[TrickMandelstam[x, {s, t, u, 0}]]],
  Text->{"Unphysical at all gluon lines cancelled by adding all possible ghost cross terms:", "CORRECT.", "WRONG!"}];


(* ::Subsection:: *)
(*Investigating crossing*)


crossMandelstam[expr_, newOrder_] :=
  expr /. {s -> sTemp, t -> tTemp, u -> uTemp} \
  /. {sTemp -> newOrder[[1]], tTemp -> newOrder[[2]], uTemp -> newOrder[[3]]}


(* ::Text:: *)
(*Crossing symmetry would imply that exchanging s and t in this squared amplitude:*)


GT1UI2GL3UO4 = square[{V[5], U[5]}, {V[5], U[5]}, {p2, 0, 0, 0}]


(* ::Text:: *)
(*Should give this squared amplitude:*)


GT1GL2UI3UO4


(* ::Text:: *)
(*But we see it does not*)


FeynCalc`FCCompareResults[
  crossMandelstam[GT1UI2GL3UO4, {t, s, u}],
  GT1GL2UI3UO4,
  Factoring->Function[x, Simplify[TrickMandelstam[x, {s, t, u, 0}]]],
  Text->{"Crossing symmetry with ghosts holds:", "CORRECT.", "WRONG!"}];


(* ::Text:: *)
(*Similarly exchanging s and t here:*)


GT1UI2GT3UO4


(* ::Text:: *)
(*would give:*)


GT1GT2UI3UO4


(* ::Text:: *)
(*Again this does not hold. Now we have transverse gluons, so this is not a effect of the longitudinal and timelike gluons.*)


FeynCalc`FCCompareResults[
  crossMandelstam[GT1UI2GT3UO4, {t, s, u}],
  GT1GT2UI3UO4,
  Factoring->Function[x, Simplify[TrickMandelstam[x, {s, t, u, 0}]]],
  Text->{"Crossing symmetry holds with ghosts and transverse gluons:", "CORRECT.", "WRONG!"}];


(* ::Text:: *)
(*Neither does it hold for the gluons alone:*)


GT1GL2GT3GL4 = square[{V[5], V[5]}, {V[5], V[5]}, {p2, 0, p4, 0}]


GT1GT2GL3GL4


FeynCalc`FCCompareResults[
  crossMandelstam[GT1GL2GT3GL4, {t, s, u}],
  GT1GT2GL3GL4,
  Factoring->Function[x, Simplify[TrickMandelstam[x, {s, t, u, 0}]]],
  Text->{"Crossing symmetry holds w.r.t. longitudinal and timelike gluons:", "CORRECT.", "WRONG!"}];


(* ::Text:: *)
(*So it is not a effect of the ghosts. *)
(*Lets check the case where all external particles are in the Feynman t' Hooft gauge:*)
(*Exchanging s and t in this cross term:*)


GL1UI2GL3UO4crossGL1UO2GL3UI4


(* ::Text:: *)
(*gives if crossing symmetry holds this:*)


GL1GL2UI3UO4crossGL1GL2UO3UI4


(* ::Text:: *)
(*And we see that it holds!*)


FeynCalc`FCCompareResults[
  crossMandelstam[GL1UI2GL3UO4crossGL1UO2GL3UI4, {t, s, u}],
  GL1GL2UI3UO4crossGL1GL2UO3UI4,
  Factoring->Function[x, Simplify[TrickMandelstam[x, {s, t, u, 0}]]],
  Text->{"Crossing symmetry holds w.r.t. longitudinal and timelike gluons:", "CORRECT.", "WRONG!"}];


(* ::Text:: *)
(*So the violation of crossing symmetry is due to the choice of the arbitrary light-cone gauge vector n_1 to be the explicit p_2!*)
(*(Or any other frame dependent choice of the light-cone gauge vector)*)
