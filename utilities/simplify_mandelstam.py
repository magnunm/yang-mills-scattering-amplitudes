"""
Utils for simplifying and/or transforming Mandelstam expressions:

-> Crossing a expression into all channels.
-> Attempting to bring to a standard/simple form (small powers, etc...).
"""
import sympy as sy
from unittest import TestCase
import random


def cross_into_all_channels(expression: sy.Expr, s: sy.Symbol,
                            t: sy.Symbol, u: sy.Symbol):
    """
    Cross `expression` into all possible channels.
    Returns a dictionary of the crossed expressions where the label
    is a string representing the crossing done.
    """
    S, T, U = sy.symbols("S T U")

    original_expression = expression.subs(s, S).subs(t, T).subs(u, U)
    all_crossed_expressions = {}

    permutations_of_stu = {
        "stu": [s, t, u],
        "sut": [s, u, t],
        "tsu": [t, s, u],
        "ust": [u, s, t],
        "tus": [t, u, s],
        "uts": [u, t, s]
    }

    for key, permutation in permutations_of_stu.items():
        all_crossed_expressions[key] = original_expression.subs(
            S, permutation[0]
        ).subs(
            T, permutation[1]
        ).subs(
            U, permutation[2]
        )

    return all_crossed_expressions


def sum_of_all_crossings(expression: sy.Expr, s: sy.Symbol, t: sy.Symbol,
                         u: sy.Symbol):
    """
    Compute the sum of a Mandelstam expression crossed into all
    possible channels.
    """
    all_crossings = cross_into_all_channels(expression, s, t, u)
    return sum(all_crossings.values())


def integrate_dcos_theta(expression: sy.Expr, s: sy.Symbol, t: sy.Symbol,
                         u: sy.Symbol):
    """
    Integrate a Mandelstam expression over d(cos(theta)) where theta is
    the scattering angle. This is equivalent to integrating it
        2/s dt
    from -s to 0.
    """
    expression = expression.subs(u, - s - t)
    expression = sy.simplify(expression)
    return 2 * sy.integrate(expression, (t, -s, 0)) / s


def simplify_mandelstam(expression: sy.Expr,
                        s: sy.Symbol, t: sy.Symbol, u: sy.Symbol,
                        attempt_standard_form=True,
                        reduce_large_powers=True):
    """
    Attempt to simplify a Mandelstam expression as much as possible.
    """
    # Remove the extra dependent variable so simplification
    # algorithm will work better
    expression = expression.subs(u, - s - t)
    expression = sy.simplify(expression)

    if expression.is_number:
        return expression

    # Bring back the substituted variable in order to express the expression
    # in a simpler way by
    # 1. First recoginize perfect powers (this helps prevent complicated
    # expressions in the denominator)
    expression = expression.subs([
        (s**3 + 3*s**2*t + 3*s*t**2 + t**3, - u**3),
        (s**2 + 2*s*t + t**2, u**2),
        (s + t, - u)
    ])

    if reduce_large_powers:
        # 2. Removing large powers
        # (The max and min powers here are dependent on the expression at hand
        # , a better solution is to check if a subs reduces overall powers)
        expression = sy.expand(expression)

        max_power = 10
        min_power = 3

        expression = expression.subs([
            (s**i, (- t - u) * s**(i - 1))
            for i in range(max_power, min_power, -1)
        ])
        expression = expression.subs([
            (t**i, (- s - u) * t**(i - 1))
            for i in range(max_power, min_power, -1)
        ])

        # 3. Recognizing perfect powers again
        expression = sy.factor(expression)
        expression = expression.subs([
            (s**3 + 3*s**2*t + 3*s*t**2 + t**3, - u**3),
            (s**2 + 2*s*t + t**2, u**2),
            (s + t, - u)
        ])

    if attempt_standard_form:
        # 4. Bring it on a "standard form". For example: we want to transform
        # s/u and s/t into t/u and u/t because this can lead to simplifying
        # cancellations. However we only want this transformation when these
        # factors are not part of a term where the denominator is a single
        # power of 2.
        # To do this we introduce dummy S, T, U to prevent sympy from
        # substituting too much.
        expression = sy.expand(expression)

        S, T, U = sy.symbols("S T U")

        expression = expression.subs([
            (s**-2, S**-2),
            (u**-2, U**-2),
            (t**-2, T**-2)
        ])

        expression = expression.subs(s / u, -1 - t / u)  # Prefer t/u over s/u
        expression = expression.subs(s / t, -1 - u / t)  # Prefer u/t over s/t
        expression = expression.subs(u / s, -1 - t / s)  # Prefer t/s over u/s

        # Remove the dummy symbols
        expression = expression.subs([
            (S, s), (T, t), (U, u)
        ])

        # Prefer squares over cubes
        expression = sy.expand(expression)
        expression = expression.subs([
            (t**3, (t/u + t/s - t**2/s**2) * s**2 * u)
        ])

        # Some choices for the standard form. Arbitrary.
        expression = sy.expand(expression)
        expression = expression.subs([
            (t**2/s**2, -t/s - t*u/s**2),
            (u**2/s**2, -u/s - t*u/s**2),
            (t**2/u**2, -t/u - t*s/u**2),
            (s**2/u**2, -s/u - t*s/u**2),
            (s**2/t**2, -s/t - s*u/t**2),
            (u**2/t**2, -u/t - s*u/t**2),
            (s**2/(t*u), 2 + t/u + u/t),
            (t**2/(s*u), 2 + s/u + u/s),
            (u**2/(s*t), 2 + s/t + t/s)
        ])

        # Simplify "t/u + s/u = -1" and expressions like that
        expression = expression.subs([
            (s**-2, S**-2),
            (u**-2, U**-2),
            (t**-2, T**-2)
        ])

        expression = expression.subs(s / u, -1 - t / u)
        expression = expression.subs(s / t, -1 - u / t)
        expression = expression.subs(u / s, -1 - t / s)

        expression = expression.subs([
            (S, s), (T, t), (U, u)
        ])

    return sy.expand(expression)


class SimplificationValidityTest(TestCase):
    """
    Unit tests testing that all simplifications done are valid, and that the
    simplification procedure will detect equality.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.s, self.t, self.u = sy.symbols("s t u")

        # Well known result. Here without numerical factor in front.
        self.four_gluon_squared_amplitude = (
            3
            - self.t * self.u / self.s**2
            - self.u * self.s / self.t**2
            - self.t * self.s / self.u**2)

    def test_cutler_and_sivers_4_gluon_result(self):
        """
        Sums the expressions given by Cutler and Sivers 1978
        (https://journals.aps.org/prd/pdf/10.1103/PhysRevD.17.196)
        for the parts of the squared four gluon amplitude. This
        sum should be equal to the known 4 gluon expression, but it is
        not due to a mistake in their calculation. This test checks that
        we are able to simplify their result to give the correct 4 gluon
        result pluss the additional erroneous factor 216 / (8^2 2^2).
        """
        mt = 9/8 * (17/2 - 4 * self.u * self.s / self.t**2)
        mu = 9/8 * (17/2 - 4 * self.t * self.s / self.u**2)
        ms = 9/8 * (17/2 - 4 * self.t * self.u / self.s**2)
        m4 = 9/8 * 27
        two_mt_mu = 9/16 * (15 - self.s**2 / (self.t * self.u))
        two_mt_ms = 9/16 * (15 - self.u**2 / (self.t * self.s))
        two_mu_ms = 9/16 * (15 - self.t**2 / (self.u * self.s))
        two_mt_mu_ms_m4 = 3 * 9 / 8 * (- 81 / 4)

        cutler_sivers_4_gluon_full = (
            mt + mu + ms + m4 + two_mt_mu + two_mt_ms + two_mu_ms +
            two_mt_mu_ms_m4
        )

        self.assertEqual(
            simplify_mandelstam(
                cutler_sivers_4_gluon_full
                - 9/2 * self.four_gluon_squared_amplitude
                # This numerical factor is how much Cutler and Sivers
                # are off the correct result:
                - 216 / (8**2 * 2**2),
                self.s, self.t, self.u),
            0
        )

    def test_4_gluon_helicity_method(self):
        """
        This test takes the result of the squared helicity ampltiude
        A(1_R,2_R,3_L,4_L) traced over color and checks that multiplying by
        two and adding the s <-> t and s <-> u crossings is the same as summing
        over all helicities. This follows from parity symmetry and simple
        relabelings of momenta.
        """
        result_4_gluon_helicity = (
            2 * self.s**2 / self.u**2 + 2 * self.s**2 / self.t**2 +
            4 * self.s**4 / (self.t**2 * self.u**2) +
            2 * self.s**3 / (self.u**2 * self.t) +
            2 * self.s**3 / (self.t**2 * self.u)
        )

        crossings = cross_into_all_channels(result_4_gluon_helicity,
                                            self.s, self.t, self.u)

        sum_over_helicities = simplify_mandelstam(
            2 * crossings['stu'] +
            2 * crossings['tsu'] +
            2 * crossings['uts'],
            self.s, self.t, self.u
        )

        sum_over_helicities_known = (
            16 * self.four_gluon_squared_amplitude
        )

        self.assertEqual(
            simplify_mandelstam(
                sum_over_helicities - sum_over_helicities_known,
                self.s,
                self.t,
                self.u
            ),
            0
        )

    def test_testcase_with_powers(self):
        testcase_1 = sy.expand((- self.t - self.u)**2)
        self.assertEqual(
            simplify_mandelstam(testcase_1 - self.s**2, self.s, self.t, self.u),
            0
        )

        testcase_2 = sy.expand((- self.t - self.u)**5 + self.t)
        self.assertEqual(
            simplify_mandelstam(testcase_2 - self.s**5, self.s, self.t, self.u),
            self.t
        )

        testcase_3 = sy.expand((- self.t - self.u)**11)
        self.assertEqual(
            simplify_mandelstam(testcase_3 - self.s**11, self.s, self.t, self.u),
            0
        )

    def test_random_testcases(self):
        """
        Randomly generate two expressions where the second is the same as the
        first except that random, but valid, substitutions are made along the
        way. Finally check that the program concludes correctly that the two
        expressions are still equal.
        """
        choices_of_variables = ["s", "t", "u"]
        choices_of_substitutions = [
            (self.s, - self.t - self.u),
            (self.t, - self.s - self.u),
            (self.u, - self.t - self.s)
        ]

        random.seed()

        number_of_choices = random.randint(5, 20)

        expression_1 = sy.sympify("0")
        expression_2 = sy.sympify("0")

        for _ in range(number_of_choices):
            new_term = sy.sympify(random.choice(choices_of_variables))

            while random.random() < 0.5:
                additional_var = sy.sympify(random.choice(choices_of_variables))

                if bool(random.randint(0, 1)):
                    new_term /= additional_var
                else:
                    new_term *= additional_var

            operator = sy.sympify(random.choice(["+1", "-1"]))

            expression_1 += operator * new_term

            expression_2 += operator * new_term.subs(
                *random.choice(choices_of_substitutions)
            )
            expression_2 = sy.expand(expression_2)

        print("Trial expression 1:", expression_1)
        print("Trial expression 2:", expression_2)

        self.assertEqual(
            simplify_mandelstam(expression_1 - expression_2,
                                self.s, self.t, self.u),
            0
        )


class IntegrationValidityTest(TestCase):
    """
    Unit tests for the integration of Mandelstam expressions
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.s, self.t, self.u = sy.symbols("s t u")

    def test_known_integrals(self):
        self.assertEqual(
            0,
            integrate_dcos_theta(1 - 6 * self.t * self.u / self.s**2,
                                 self.s, self.t, self.u))

        self.assertEqual(
            0,
            integrate_dcos_theta(1 + 2 * self.t / self.s,
                                 self.s, self.t, self.u))


if __name__ == '__main__':
    import argparse

    sy.init_printing()

    parser = argparse.ArgumentParser(
        description='Compare two Mandelstam expressions, or simplify'\
        ' if single arguement.'
    )
    parser.add_argument('expr1', metavar='expr1', type=str,
                        help='First Mandelstam expression')
    parser.add_argument('expr2', metavar='expr2', type=str, nargs='?',
                        help='Second Mandelstam expression', default="0")

    args = parser.parse_args()

    s, t, u = sy.symbols("s t u")

    expr1 = sy.sympify(args.expr1)
    expr2 = sy.sympify(args.expr2)

    if expr2 == 0:
        print("Simplified expression =")
        sy.pprint(simplify_mandelstam(expr1, s, t, u))
    else:
        difference = simplify_mandelstam(expr1 - expr2, s, t, u)
        ratio = simplify_mandelstam(expr1/expr2, s, t, u)

        print("Difference: ")
        sy.pprint(difference)
        print("\n")
        print("Ratio: ")
        sy.pprint(ratio)
