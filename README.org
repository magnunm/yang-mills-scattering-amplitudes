#+TITLE: Symbolic computation of squared QCD scattering amplitudes
#+OPTIONS: toc:nil author:nil date:nil
#+LATEX_HEADER: \usepackage[margin=4cm]{geometry}
#+LATEX_HEADER: \sloppy

This file is best read in the pdf version.

* Acknowledgments 

The symbolic calculations in this repository are made possible by the
[[https://www.nikhef.nl/~form/][FORM]] symbolic manipulation system by Jos Vermaseren of Nikhef, as well
as the [[https://feyncalc.github.io/][FeynCalc]] (by Rolf Mertig, Frederik Orellana and Vladyslav
Shtabovenko) and [[http://www.feynarts.de/][FeynArts]] (by Thomas Hahn) packages for Wolfram
Mathematica. Also in the Python scripts [[https://www.sympy.org/en/index.html][sympy]] is used.

* Symbolic tree level calculations of squared QCD amplitudes

The =quark-gluon-scattering= and =gluon-gluon-scattering= directory
contains scripts to calculate
\[ 
    \mathrm{tr} |\mathcal{A}|^2
\]
for the case of \(q \bar{q} \to gg\) and \(gg \to gg\)
respectively. Different gauge choices are made for the external lines:
using the light-cone/axial gauge external the gluons are transverse,
but this requires picking the center-of-momentum frame. Alternatively
one can use the covariant Feynman-t' Hooft gauge also for external
lines, but then one has to include Faddeev-Popov ghosts also as
external particles. A cancellation between the ghost and gluon terms
ensures that the final result is gauge-independent.  For the 4 gluon
case the Mathematica script
=gluon-gluon-scattering/gluon-gluon-scattering-ghost-cancellation.wl=
checks all the cancellations at hand in different combinations of
gauge choices for the external lines.

The calculation of the 4-gluon case using the Feynman-t' Hooft gauge
on all external lines was done by R. Cutler and D. Sivers in their
1977 paper [[https://doi.org/10.1103/PhysRevD.17.196][Quantum Chromodynamic Gluon Contributions to Large p(T)
Reactions]]. Their final expression is however off by a factor \(216
g_s^4 / (8^2 2^2)\). The calculation using transverse external states
has been done by many authors, the first published calculation known
to me is [[https://doi.org/10.1016/0370-2693(77)90528-7][Combridge, Kripfganz and Ranft (1977)]].

* Mandelstam utility

The =utilities= directory contains a Python utility for working with
expressions with Mandelstam variables. It is useful for quickly
comparing two Mandelstam expressions, or for simplifying a Mandelstam
expression. In addition to this it can cross a Mandelstam expression
into all possible channels, or integrate it
\(\mathrm{d}(\cos \theta)\).
