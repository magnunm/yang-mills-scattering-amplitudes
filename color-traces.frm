* Some example SU(N) color-traces calculated using the Fierz identity.
Off statistics;
* Define our own delta due to variable dimension N
* T are the SU(N) generators
Tensors T, del(symmetric);
* a,...e for generator labels
* i1...i10 for matrix indices
Indices a, b, c, d, e,
	i1, ..., i10;
Symbols N, n;

* 5 traces / products of traces, matrix indices are written out.
Local tr1 = T(a, i1, i2) * T(b, i2, i3) * T(c, i3, i4) * T(d, i4, i1) *
      	    T(d, i5, i6) * T(c, i6, i7) * T(b, i7, i8) * T(a, i8, i5);

Local tr2 = T(a, i1, i2) * T(b, i2, i3) * T(c, i3, i4) * T(d, i4, i1) *
      	    T(d, i5, i6) * T(c, i6, i7) * T(a, i7, i8) * T(b, i8, i5);

Local tr3 = T(a, i1, i2) * T(b, i2, i3) * T(c, i3, i4) * T(d, i4, i1) *
      	    T(d, i5, i6) * T(a, i6, i7) * T(b, i7, i8) * T(c, i8, i5);

Local tr4 = T(a, i1, i2) * T(a, i2, i3) * T(b, i3, i4) * T(b, i4, i1);

Local tr5 = T(a, i1, i2) * T(b, i2, i3) * T(a, i3, i4) * T(b, i4, i1);

repeat;
* Apply Fierz identity
    id T(a?, i1?, i2?) * T(a?, i3?, i4?) = (del(i1, i4) * del(i2, i3) -
                                           (del(i1, i2) * del(i3, i4)) / N )/2;
* Contract kroenecker deltas
    id del(i1?, i2?) * T(a?, i2?, i3?) = T(a, i1, i3);
    id del(i1?, i2?) * T(a?, i3?, i2?) = T(a, i3, i1);
    id del(i1?, i2?) * del(i2?, i3?) = del(i1, i3);
    id del(i1?, i1?) = N;
endrepeat;

Print;
.sort

** Special case N = 3
id N^n? = 3^n;
Print;
.end
